'use strict';

const { Cluster } = require('puppeteer-cluster');


exports.crawl = async (keyword, onSuccess) => {

    const cluster = await Cluster.launch({
        concurrency: Cluster.CONCURRENCY_CONTEXT,
        maxConcurrency: 2,
        puppeteerOptions: {     // ONLY FOR AWS ENVIRONMENT
            executablePath: '/usr/bin/google-chrome-stable', 
            headless: true, 
            args: ['--no-sandbox', '--disable-setuid-sandbox']
        }
    });

    let tokopediaProducts = [];
    let alibabaProducts = [];

    const extractTokopedia = async ({page, data: url}) => {
        await page.setViewport({width: 1680, height: 9050});    // set more height coz tokopedia used 'lazy load' on > 20th items. alibaba too
        await page.setUserAgent('“Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36');
        await page.goto(url, {waitUntil: 'load', timeout: 0});

        console.log('crawling tokopedia...');
        const products = await page.evaluate(() => {

            let data = [];

            const allSelector = document.querySelectorAll('div._1a-JxuBv');
            for(let divItem of allSelector){

                let productURL = divItem.querySelector('div._2OBup6Zd a').getAttribute('href');
                let title = divItem.querySelector('div.S4G9wXKj h3').innerText;
                let price = divItem.querySelector('span._3fNeVBgQ span').innerText;
                let photo = divItem.querySelector('div._3lM9QIdS img').getAttribute('src');
                let seller = divItem.querySelector('span._1GDgKs4K').innerText;
                let sellerURL = null;

                let product = {
                    product_url: productURL,
                    title: title,
                    price: 0,
                    currency: 'IDR',
                    photo: photo,
                    seller: seller,
                    seller_url: sellerURL,
                    platform: 'tokopedia'
                }
                

                data.push(product);
            }

            return data;
        });

        tokopediaProducts = products;

    }


    const extractAlibaba = async ({page, data: url}) => {
        await page.setViewport({width: 1680, height: 9050});
        await page.setUserAgent('“Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36');
        await page.goto(url, {waitUntil: 'load', timeout: 0});


        console.log('crawling alibaba...');
        const products = await page.evaluate(() => {

            let data = [];

            // check layout
            const isCategoryLayout = document.querySelector('.l-theme-card-box') !== null;
            if(isCategoryLayout){
                const allSelector = document.querySelectorAll('div.m-product-item');
                for(let divItem of allSelector){
                    const itemContent = divItem.querySelector('div.item-content');
                    if(itemContent === null){  // avoid 'Featured Discover' section
                        continue;
                    }

                    let productURL = itemContent.querySelector('div.title-wrap h2.title a').getAttribute('href');
                    let title = itemContent.querySelector('div.title-wrap h2.title a').innerText;
                    let price = itemContent.querySelector('div.price b').innerText;
                    let photo = itemContent.querySelector('div.util-valign img.util-valign-inner').getAttribute('src');
                    let seller = itemContent.querySelector('div.item-extra div.extra-wrap a').innerText;
                    let sellerURL = itemContent.querySelector('div.item-extra div.extra-wrap a').getAttribute('href');

                    let product = {
                        product_url: productURL,
                        title: title,
                        price: 0,
                        currency: 'USD',
                        photo: photo,
                        seller: seller,
                        seller_url: sellerURL,
                        platform: 'alibaba'
                    }

                    data.push(product);
                }
            }
            else {
                const allSelector = document.querySelectorAll('div.organic-gallery-offer-outter');
                for(let divItem of allSelector){

                    const titleSection = divItem.querySelector('div.organic-offer-wrapper div.organic-gallery-offer-section__title');
                    if(titleSection === null){  // avoid 'Featured Discover' section
                        continue;
                    }

                    let productURL = titleSection.querySelector('a.organic-gallery-title').getAttribute('href');
                    let title = titleSection.querySelector('a.organic-gallery-title p').innerText;
                    let price = divItem.querySelector('div.organic-gallery-offer-section__price')
                        .querySelector('p.gallery-offer-price span').innerText;
                    let photo = divItem.querySelector('a.organic-gallery-offer__img-section')
                        .querySelector('div.seb-img-switcher__imgs img').getAttribute('src');
                    let seller = divItem.querySelector('a.organic-gallery-offer__seller-company').innerText;
                    let sellerURL = divItem.querySelector('a.organic-gallery-offer__seller-company').getAttribute('href');

                    let product = {
                        productURL: productURL,
                        title: title,
                        price: 0,
                        photo: photo,
                        seller: seller,
                        sellerURL: sellerURL,
                        platform: 'alibaba'
                    }

                    data.push(product);
                }
            }

            return data;
        });

        alibabaProducts = products;
    }


    // RUN QUEUE...
    cluster.queue(tokopediaURL(keyword), extractTokopedia);
    cluster.queue(alibabaURL(keyword), extractAlibaba);

    await cluster.idle();
    await cluster.close();

    onSuccess(tokopediaProducts, alibabaProducts);

}

const alibabaURL = (keyword) => {
    const formatedKeyword = keyword.split(' ').join('+');
    return `https://www.alibaba.com/trade/search?IndexArea=product_en&SearchText=${formatedKeyword}`;
}

const tokopediaURL = (keyword) => {
    const formatedKeyword = keyword.split(' ').join('%20');
    return `https://www.tokopedia.com/search?st=product&q=${formatedKeyword}`;
}


// RUN CRAWL
// crawl('HP spectre', (tokopediaProducts, alibabaProducts) => {
//     console.log('OUTPUT:');
//     console.log(tokopediaProducts);
//     console.log(alibabaProducts);
// })

