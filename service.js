'use strict';

const crawler = require('./crawler');
const models = require('./models');


exports.search = async (keyword, onSuccess) => {

    const existingKeywords = await models.Keyword.findAll({
        where: {word: keyword},
        include: models.Product
    });

    if(existingKeywords.length > 0){
        onSuccess(existingKeywords[0].Products);
        return;
    }



    crawler.crawl(keyword, async (tokopedia, alibaba) => {
        try{
            const newKeyword = await models.Keyword.create({
                word: keyword
            });


            if(newKeyword){
                let products = tokopedia;
                products.push(...alibaba);

                products.forEach(function (element) {
                    element.keyword_id = newKeyword.id;
                  });

                const data = await models.Product.bulkCreate(products);
                console.log(data);

                onSuccess(products);
            }
        }
        catch(error){
            console.log(error);
        }  
    });

}