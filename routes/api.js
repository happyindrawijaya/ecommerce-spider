
const express = require('express');
const router = express.Router();

const service = require('../service');


router.get('/crawl', async (req, res) => {

    const keyword = req.query.search;
    if(keyword === undefined){
        res.json({"keyword": "", "products": []});
    }

    const lowKeyword = keyword.toLowerCase();
    
    service.search(lowKeyword, (products) => {
        res.json({"keyword": lowKeyword, "products": [products], "server_current_time": new Date()});
    })

});

module.exports = router;
