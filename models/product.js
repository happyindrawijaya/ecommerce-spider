'use strict';

module.exports = (sequelize, DataTypes) => {

    const Product = sequelize.define('Product', {
        title: DataTypes.STRING,
        price: DataTypes.INTEGER,
        currency: DataTypes.STRING,
        description: DataTypes.TEXT,
        photo: DataTypes.STRING,
        seller: DataTypes.STRING,
        seller_url: DataTypes.TEXT,
        product_url: DataTypes.TEXT,
        platform: DataTypes.STRING
    }, {});

    Product.associate = function(models) {
        models.Product.belongsTo(models.Keyword, {
            onDelete: 'CASCADE',
            foreignKey: 'keyword_id',
        });
    };

    return Product;
};