'use strict';
module.exports = (sequelize, DataTypes) => {

    const Keyword = sequelize.define('Keyword', {
        word: DataTypes.STRING
    }, {});

    Keyword.associate = function(models) {
        models.Keyword.hasMany(models.Product, { foreignKey: 'keyword_id' });
    };
    return Keyword;
};