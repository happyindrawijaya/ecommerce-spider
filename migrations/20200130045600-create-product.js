'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Products', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        title: {
            type: Sequelize.STRING
        },
        price: {
            type: Sequelize.INTEGER
        },
        currency: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.TEXT
        },
        photo: {
            type: Sequelize.STRING
        },
        seller: {
            type: Sequelize.STRING
        },
        seller_url: {
            type: Sequelize.TEXT
        },
        product_url: {
            type: Sequelize.TEXT
        },
        platform: {
            type: Sequelize.STRING
        },
        createdAt: {
            allowNull: false,
            type: Sequelize.DATE
        },
        updatedAt: {
            allowNull: false,
            type: Sequelize.DATE
        },
        keyword_id: {
            type: Sequelize.INTEGER,
            onDelete: 'CASCADE',
            allowNull: false,
            references: {
                model: 'Keywords',
                key: 'id'
            }
        }
        });
    },
    down: (queryInterface, Sequelize) => {
        // return queryInterface.dropTable('Products');
    }
};